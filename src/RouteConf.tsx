import React from "react";
import {RouteConfig} from "react-router-config";
import { Redirect } from 'react-router';

import Navigation from "./components/Navigation/Navigation";
import Round from "./components/Round/Round";
import FinalBoard from "./components/FinalBoard/FinalBoard";
import NotFoundPage from "./components/NotFound/NotFoundPage";
import RoundRedirect from "./components/Round/RoundRedirect";

export const routes: RouteConfig[] = [
    {
        component: Navigation,
        routes: [
            {
                path: "/",
                exact: true,
                component: () => <Redirect to="/round/"/>
            },
            {
                path: "/round",
                exact: true,
                component: RoundRedirect
            },
            {
                path: "/round/:id",
                component: Round,
            },
            {
                path: "/final-board",
                component: FinalBoard
            },
            {
                path: "/not-found",
                component: NotFoundPage
            }
        ]
    }
];