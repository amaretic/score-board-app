import React from 'react';
import {ThemeProvider} from '@material-ui/styles';
import {BrowserRouter as Router} from 'react-router-dom';
import {renderRoutes} from "react-router-config";
import {Provider} from 'react-redux'

import {theme} from "./Theme";
import {routes} from "./RouteConf";
import store from "./reducers/store";

const App: React.FC = () => {
    return (
        <Provider store={store}>
            <Router>
                <ThemeProvider theme={theme}>
                    {renderRoutes(routes)}
                </ThemeProvider>
            </Router>
        </Provider>
    );
}

export default App;