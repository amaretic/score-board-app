import createMuiTheme from "@material-ui/core/styles/createMuiTheme";
import {PaletteColorOptions} from "@material-ui/core/styles/createPalette";

const primaryPalette: PaletteColorOptions = {
    light: "#ffe5e3",
    dark: "#aa2e25",
    main: "#f44336"
}

const secondaryPalette: PaletteColorOptions = {
    light: "#595959",
    dark: "#212121",
    main: "#303030"
}

export const theme = createMuiTheme({
    palette: {
        primary: primaryPalette,
        secondary: secondaryPalette
    },
    overrides: {
        MuiListItem: {
            root: {
               '&.active': {
                   background: primaryPalette.light
               }
            }
        }
    }
});