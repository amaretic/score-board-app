import {combineReducers} from "redux";
import {sidebarReducer} from "./Sidebar.reducer";

export const rootReducer = combineReducers({
    sidebarState: sidebarReducer
});

export type AppState = ReturnType<typeof rootReducer>