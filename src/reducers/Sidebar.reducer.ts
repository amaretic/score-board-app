export interface ISidebarState {
    isOpen: boolean
}

export const initialSidebarState: ISidebarState = {
    isOpen: true
};

export const TOGGLE_SIDEBAR = 'TOGGLE_SIDEBAR';

interface ToggleSidebar {
    type: typeof TOGGLE_SIDEBAR
}

export type SidebarActionTypes = ToggleSidebar;

export const toggleSidebar = (): SidebarActionTypes => {
    return {
        type: TOGGLE_SIDEBAR
    }
}

export const sidebarReducer = (state: ISidebarState = initialSidebarState, action: SidebarActionTypes): ISidebarState => {
    switch (action.type) {
        case TOGGLE_SIDEBAR:
            return {...state, isOpen: !state.isOpen};
        default:
            return state;
    }
}