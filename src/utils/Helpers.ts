import {default as jsonData} from '../assets/data.json';
import _ from "lodash";

export interface ITeam {
    [teamName: string]: number
}

export interface IMatch {
    homeTeam: ITeam,
    guestTeam: ITeam
}

export interface IRound {
    round: number,
    matches: IMatch[]
}

export interface IMatchDetails {
    homeTeamName: string,
    guestTeamName: string,
    homeTeamScore: string,
    guestTeamScore: string,
    isDraw: boolean,
    isHomeWinner: boolean
}

export const getRoundNumbers = () => {
    let rounds = _.flatMap(jsonData, (value: IRound) => {
        return value.round
    });
    _.reverse(rounds);
    return rounds;
}

export const getRoundDetails = (round: string): IRound | any => {
    return _.find(jsonData, {round: parseInt(round) as number});
};

export const getLastRoundNum = (): number => {
    let lastRoundNum: IRound = _.maxBy(jsonData, function (round: IRound) {
        return round.round;
    });
    return lastRoundNum.round;
}

export const getAllRoundsData = (): IRound[] => {
    console.log("")
    return jsonData as IRound[];
}

export const getMatchDetails = (match: IMatch): IMatchDetails => {
    const homeTeamName = Object.keys(match)[0];
    const homeTeamScore = Object.values(match)[0];
    const guestTeamName = Object.keys(match)[1];
    const guestTeamScore = Object.values(match)[1];
    const isDraw = homeTeamScore === guestTeamScore;
    const isHomeWinner = homeTeamScore > guestTeamScore;
    return {
        homeTeamName,
        homeTeamScore,
        guestTeamName,
        guestTeamScore,
        isDraw,
        isHomeWinner
    }
}