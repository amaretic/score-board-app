import {Theme} from '@material-ui/core/styles';

export const useNotFoundStyle = (theme: Theme) => ({
    dialog: {
        justifyContent: "center",
        alignItems: "center"
    },
    container: {
        maxWidth: 520,
        width: '100%',
        justifyContent: "center",
    },
    item: {
        height: 200
    },
    typographyH1: {
        fontSize: 236,
        fontWeight: 200,
        margin: 0,
        [theme.breakpoints.down("sm")]: {
            fontSize: 148
        },
        [theme.breakpoints.down("xs")]: {
            fontSize: 86
        }
    },
    typographyH2: {
        fontSize: 28,
        fontWeight: 400,
        bottom: -10,
        left: 0,
        right: 0,
        padding: "10px 5px",
        background: "#fff",
        [theme.breakpoints.down("sm")]: {
            bottom: 60,
        },
        [theme.breakpoints.down("xs")]: {
            fontSize: 16,
            bottom: 110,
        }
    },
    itemButton: {
        [theme.breakpoints.up("sm")]: {
            marginTop: 30
        }
    }
});