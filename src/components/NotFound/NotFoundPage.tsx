import React from "react";
import {NavLink} from "react-router-dom";
import {withStyles} from '@material-ui/core/styles';
import Dialog from "@material-ui/core/Dialog";
import Grid from "@material-ui/core/Grid";
import {Typography} from "@material-ui/core";
import Button from "@material-ui/core/Button";

import {useNotFoundStyle} from "./NotFoundPageStyle";

interface INotFoundPageProps {
    classes: {
        container: string,
        item: string,
        typographyH1: string,
        typographyH2: string,
        dialog: string,
        itemButton: string
    }
}

const NotFoundPage: React.FC<INotFoundPageProps> = (props) => {
    return (
        <Dialog open fullScreen PaperProps={{classes: {root: props.classes.dialog}}}>
            <Grid container className={props.classes.container}>
                <Grid item className={props.classes.item} style={{position: "relative"}}>
                    <Typography variant={"h1"} style={{textTransform: "uppercase"}}
                                className={props.classes.typographyH1}>Oops!</Typography>
                    <Typography style={{position: "absolute"}} variant={"h2"} className={props.classes.typographyH2}>404
                        - The Page can't be found</Typography>
                </Grid>
                <Grid item className={props.classes.itemButton}>
                    <Button variant="contained" color={"primary"} component={NavLink} to={"/"}>
                        Go TO Homepage
                    </Button>
                </Grid>
            </Grid>
        </Dialog>
    );
}

export default withStyles(useNotFoundStyle)(NotFoundPage);