import React from "react";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";

const FinalBoardHeader: React.FC = () => {
    const tableCellNames = [
        "no",
        "name",
        "MP", // match played
        "W", // win
        "D", // draw
        "L", // lose
        "G", // goal diff format GIVE:GET
        "G Diff", // goal diff
        "Pts", // points
        "last 5 matches", // empty for show last 5 games

    ]
    return (
        <TableHead>
            <TableRow>
                {tableCellNames.map((value: string, index) => {
                    return <TableCell key={index} style={{textTransform: "capitalize"}}>
                        {value}
                    </TableCell>
                })}
            </TableRow>
        </TableHead>
    );
}

export default FinalBoardHeader;