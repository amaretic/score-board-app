import React from "react";
import _ from "lodash";

import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import {ITeamScore} from "./FinalBoard";
import Last5GamesScore from "./Last5GamesScore";

interface IFinalBoardRow {
    team: ITeamScore,
    position: number,
}

const FinalBoardRow: React.FC<IFinalBoardRow> = (props) => {
    let last5games = props.team.last5GamesScore.slice(Math.max(props.team.last5GamesScore.length - 5, 1));
    _.reverse(last5games);

    return (
        <TableRow hover tabIndex={-1}>
            <TableCell>{props.position}</TableCell>
            <TableCell>{props.team.name}</TableCell>
            <TableCell>{props.team.matchPlayed}</TableCell>
            <TableCell>{props.team.winMatches}</TableCell>
            <TableCell>{props.team.drawMatches}</TableCell>
            <TableCell>{props.team.loseMatches}</TableCell>
            <TableCell>{props.team.numOfGoalsGive + ":" + props.team.numOfGoalsGet}</TableCell>
            <TableCell>{props.team.goalDiff}</TableCell>
            <TableCell>{props.team.numOfPoints}</TableCell>
            <TableCell style={{whiteSpace: "nowrap"}}><Last5GamesScore lastScores={last5games}/></TableCell>
        </TableRow>
    );
}

export default FinalBoardRow;