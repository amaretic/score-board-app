import React from "react";
import _ from "lodash";
import TableBody from "@material-ui/core/TableBody";
import Card from "@material-ui/core/Card";
import Table from "@material-ui/core/Table";
import Grow from "@material-ui/core/Grow";
import {Theme, withStyles} from '@material-ui/core/styles';

import {getAllRoundsData, getMatchDetails, IMatch, IMatchDetails, IRound} from "../../utils/Helpers";
import FinalBoardHeader from "./FinalBoardHeader";
import FinalBoardRow from "./FinalBoardRow";

export type IScoreStatus = "W" | "L" | "D";

export interface ITeamScore {
    name: string,
    matchPlayed: number,
    winMatches: number,
    loseMatches: number,
    drawMatches: number,
    numOfGoalsGive: number,
    numOfGoalsGet: number,
    goalDiff: number,
    numOfPoints: number,
    last5GamesScore: IScoreStatus[];
}

const useFinalBoardStyle = (theme: Theme) => ({
    card: {
        maxHeight: `calc(100vh - 110px)`,
        overflow: 'auto'
    }
});

interface IFinalBoard {
    classes: {
        card: string
    }
}

const FinalBoard: React.FC<IFinalBoard> = ({classes}) => {
    const roundsData = getAllRoundsData();
    let teamData: ITeamScore[] = [];

    const initTeamScore = (teamName: string): ITeamScore => {
        return {
            name: teamName,
            winMatches: 0,
            loseMatches: 0,
            drawMatches: 0,
            numOfGoalsGive: 0,
            numOfGoalsGet: 0,
            goalDiff: 0,
            numOfPoints: 0,
            matchPlayed: 0,
            last5GamesScore: []
        }
    };

    const resolvePoints = (team: ITeamScore, matchDetails: IMatchDetails) => {
        if (matchDetails.isDraw) {
            team.drawMatches++;
            team.numOfPoints++;
            team.last5GamesScore.push("D");
        } else if ((matchDetails.isHomeWinner && matchDetails.homeTeamName === team.name) ||
            (!matchDetails.isHomeWinner && matchDetails.guestTeamName === team.name)) {
            team.winMatches++;
            team.numOfPoints += 3;
            team.last5GamesScore.push("W");
        } else {
            team.loseMatches++;
            team.last5GamesScore.push("L");
        }
        team.matchPlayed++;
        team.numOfGoalsGet += parseInt(matchDetails.guestTeamScore);
        team.numOfGoalsGive += parseInt(matchDetails.homeTeamScore);
        team.goalDiff = team.numOfGoalsGive - team.numOfGoalsGet;
    }

    const findTeam = (teamData: ITeamScore[], teamName: string): ITeamScore | undefined => {
        return _.find(teamData, {name: teamName});
    }

    _.map(roundsData, (round: IRound) => {
        _.map(round.matches, (match: IMatch) => {
            const matchDetails = getMatchDetails(match);
            let homeTeam = findTeam(teamData, matchDetails.homeTeamName);
            if (!homeTeam) {
                teamData.push(initTeamScore(matchDetails.homeTeamName));
                homeTeam = findTeam(teamData, matchDetails.homeTeamName);
            }
            let guestTeam = findTeam(teamData, matchDetails.guestTeamName);
            if (!guestTeam) {
                teamData.push(initTeamScore(matchDetails.guestTeamName));
                guestTeam = findTeam(teamData, matchDetails.guestTeamName);
            }
            if (homeTeam) {
                resolvePoints(homeTeam, matchDetails);
            }
            if (guestTeam) {
                resolvePoints(guestTeam, matchDetails);
            }
        })
    });
    teamData = _.orderBy(teamData, ['numOfPoints', "goalDiff"], ["desc", "desc"]);

    return (
        <Grow in>
            <Card elevation={2} className={classes.card}>
                {/* TODO use mui-datatable for responsive design */}
                <Table stickyHeader>
                    <FinalBoardHeader/>
                    <TableBody>
                        {teamData.map((team: ITeamScore, i) => {
                            return <FinalBoardRow team={team} key={i} position={i + 1}/>
                        })}
                    </TableBody>
                </Table>
            </Card>
        </Grow>
    );
}

export default withStyles(useFinalBoardStyle)(FinalBoard);