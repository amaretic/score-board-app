import React from "react";
import {Theme, withStyles} from '@material-ui/core/styles';
import Box from "@material-ui/core/Box";
import Chip from "@material-ui/core/Chip";

import {IScoreStatus} from "./FinalBoard";

const useScoreStyle = (theme: Theme) => ({
    chip: {
        margin: 5,
        width: 25
    }
});

interface ILast5GamesScore {
    lastScores: IScoreStatus[],
    classes: {
        chip: string
    }
}
const Last5GamesScore: React.FC<ILast5GamesScore> = ({lastScores, classes}) => {
    return (
        <Box component={"span"}>
            {lastScores.map((score: IScoreStatus, index) => {
                let chipColor;
                switch (score) {
                    case "W": chipColor = "#5cb85c"; break;
                    case "D": chipColor = "#f0ad4e"; break;
                    case "L": chipColor = "#d9534f"; break;
                    default: chipColor = "#fff";
                }
                return (
                    <Chip key={index} size={"small"} label={score} style={{backgroundColor: chipColor, color: "#fff"}} className={classes.chip}/>
                )
            })}
        </Box>
    );
}

export default withStyles(useScoreStyle)(Last5GamesScore);