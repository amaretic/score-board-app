import React from "react";
import {Redirect} from "react-router";

import {getLastRoundNum} from "../../utils/Helpers";

const RoundRedirect: React.FC = () => {
    const getLastRoundNumber = getLastRoundNum();
    const redirectRoute = getLastRoundNumber ? "/round/" + getLastRoundNumber : "/not-found";
    return (
        <Redirect to={redirectRoute}/>
    );
}

export default RoundRedirect;