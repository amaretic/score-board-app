import React from "react";
import _ from "lodash";
import {Redirect, RouteComponentProps, withRouter} from "react-router";

import Card from "@material-ui/core/Card";
import Grow from "@material-ui/core/Grow";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import {Theme, withStyles} from '@material-ui/core/styles';
import Grid from "@material-ui/core/Grid";

import {getRoundDetails, IMatch} from "../../utils/Helpers";
import MatchDetails from "./MatchDetails";

interface IRoundRouteProps {
    id: string
}

interface IRoundProps {
    classes: {
        card: string
    }
}

const useRoundStyle = (theme: Theme) => ({
    card: {
        margin: "auto",
        maxWidth: 600
    }
});

const Round: React.FC<RouteComponentProps<IRoundRouteProps> & IRoundProps> = (props) => {
    let {id} = props.match.params;
    let roundDetails = getRoundDetails(id);
    if(roundDetails !== undefined) {
        return (
            <Grow in key={id}>
                <Card className={props.classes.card}>
                    <CardHeader title={"Round " + id}/>
                    <CardContent>
                        <Grid container>
                            {
                                _.map(roundDetails.matches, (match: IMatch, index) => {
                                    return <MatchDetails match={match} key={index}/>
                                })
                            }
                        </Grid>
                    </CardContent>
                </Card>
            </Grow>
        );
    } else {
        return <Redirect to={'/not-found'}/>
    };
}

export default withStyles(useRoundStyle)(withRouter(Round));