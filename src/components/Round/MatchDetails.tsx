import React from "react";
import Card from "@material-ui/core/Card";
import List from "@material-ui/core/List";
import ListItemText from "@material-ui/core/ListItemText";
import ListItem from "@material-ui/core/ListItem";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";

import {IMatch, getMatchDetails} from "../../utils/Helpers";

interface IMatchDetailsProps {
    match: IMatch
}

const MatchDetails: React.FC<IMatchDetailsProps> = (props) => {
    const matchDetails = getMatchDetails(props.match);
    return (
        <Grid item md={6} sm={12}>
            <Box m={2}>
                <Card elevation={2}>
                    <List>
                        <ListItem disabled={!matchDetails.isDraw && !matchDetails.isHomeWinner}>
                            <ListItemText primary={matchDetails.homeTeamName}/> {matchDetails.homeTeamScore}
                        </ListItem>
                        <ListItem disabled={!matchDetails.isDraw && matchDetails.isHomeWinner}>
                            <ListItemText primary={matchDetails.guestTeamName}/> {matchDetails.guestTeamScore}
                        </ListItem>
                    </List>
                </Card>
            </Box>
        </Grid>
    );
}

export default MatchDetails;