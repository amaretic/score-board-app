import React, {Dispatch} from "react";
import {RouteComponentProps, withRouter} from "react-router";
import {renderRoutes} from "react-router-config";
import {connect} from "react-redux";

import CssBaseline from '@material-ui/core/CssBaseline';
import Drawer from '@material-ui/core/Drawer';
import Divider from '@material-ui/core/Divider';
import {withStyles} from '@material-ui/styles';
import withWidth, {isWidthUp, WithWidth} from '@material-ui/core/withWidth';

import {useNavStyle} from "./NavigationStyle";
import TopBar from "./components/TopBar";
import SidebarHeader from "./components/SidebarHeader";
import SidebarList from "./components/SidebarList";
import {AppState} from "../../reducers/rootReducer";
import {SidebarActionTypes, toggleSidebar} from "../../reducers/Sidebar.reducer";

interface INavigationProps extends WithWidth {
    isSidebarOpen: boolean
    toggleSidebar: () => void
    classes: {
        root: string
        appBarShift: string
        drawer: string
        drawerPaper: string
        content: string
        contentShift: string,
        toolbar: string
    }
    // don't know why but this fix stuff in renderRoutes
    route?: any;
}

const Navigation: React.FC<RouteComponentProps & INavigationProps> = (props) => {
    const isBigScreen = isWidthUp("sm", props.width);
    return (
        <div className={props.classes.root}>
            <CssBaseline/>
            <TopBar toggleDrawer={props.toggleSidebar}/>
            <Drawer
                className={props.classes.drawer}
                variant={isBigScreen ? "persistent" : "temporary"}
                onClose={props.toggleSidebar}
                anchor="left"
                open={props.isSidebarOpen}
                classes={{
                    paper: props.classes.drawerPaper,
                }}
            >
                <SidebarHeader isBigScreen={isBigScreen}/>
                <Divider/>
                <SidebarList/>
            </Drawer>
            <main
                className={props.classes.content + (isBigScreen && props.isSidebarOpen ? " " + props.classes.contentShift : "")}
            >
                <div className={props.classes.toolbar}/>
                {renderRoutes(props.route.routes)}
            </main>
        </div>
    )
}
const mapStateToProps = (state: AppState) => ({
    isSidebarOpen: state.sidebarState.isOpen,
});

const mapDispatchToProps = (dispatch: Dispatch<SidebarActionTypes>) => {
    return {
        toggleSidebar: () => dispatch(toggleSidebar())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withWidth()(withStyles(useNavStyle)(withRouter(Navigation))));