import React from "react";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import {Theme, withStyles} from '@material-ui/core/styles';
import AppBar from "@material-ui/core/AppBar/AppBar";

import MenuIcon from '@material-ui/icons/Menu';

interface ITopBarProps {
    toggleDrawer: () => void,
    classes: {
        menuButton: string
        toolbar: string
    }
}

const useTopBarStyle = (theme: Theme) => ({
    menuButton: {
        marginRight: theme.spacing(2),
    },
    toolbar: {
        paddingLeft: 16,
        paddingRight: 16
    }
});

const TopBar: React.FC<ITopBarProps> = (props) => {
    return (
        <AppBar
            position="fixed"
        >
            <Toolbar className={props.classes.toolbar}>
                <IconButton
                    color="inherit"
                    onClick={props.toggleDrawer}
                    edge="start"
                    className={props.classes.menuButton}
                >
                    <MenuIcon/>
                </IconButton>
                <Typography variant="h6" noWrap>
                    Score board 2016/2017
                </Typography>
            </Toolbar>
        </AppBar>
    );
}

export default withStyles(useTopBarStyle)(TopBar);