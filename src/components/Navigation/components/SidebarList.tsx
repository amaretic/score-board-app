import React from "react";
import _ from "lodash";
import {NavLink} from "react-router-dom";

import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import List from "@material-ui/core/List";

import {getRoundNumbers} from "../../../utils/Helpers";

const SidebarList: React.FC = () => {
    const getNumOfRounds = getRoundNumbers();
    return (
        <List>
            {_.map(getNumOfRounds, (text, index) => (
                <ListItem button key={index} component={NavLink} to={"/round/" + text}>
                    <ListItemText primary={"Round " + text}/>
                </ListItem>
            ))}
        </List>
    );
}

export default SidebarList;