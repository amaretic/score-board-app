import React, {Dispatch} from "react";
import {NavLink} from "react-router-dom";
import {connect} from "react-redux";

import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import IconButton from "@material-ui/core/IconButton";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";

import ClearIcon from '@material-ui/icons/Clear';
import TableIcon from '@material-ui/icons/TableChart';
import {SidebarActionTypes, toggleSidebar} from "../../../reducers/Sidebar.reducer";

interface ISidebarHeaderProps {
    isBigScreen: boolean,
    toggleSidebar: () => void
}

const SidebarHeader: React.FC<ISidebarHeaderProps> = (props) => {
    return (
        <div>
            {/*<div className={props.classes.drawerHeader}>*/}
            <List>
                {!props.isBigScreen &&
                <ListItem style={{display: 'flex', justifyContent: 'flex-end'}}>
                    <IconButton onClick={props.toggleSidebar}>
                        <ClearIcon/>
                    </IconButton>
                </ListItem>}
                <ListItem button component={NavLink} to="/final-board">
                    <ListItemIcon><TableIcon/></ListItemIcon>
                    <ListItemText primary="Final score board"/>
                </ListItem>
            </List>
        </div>
    );
}
const mapDispatchToProps = (dispatch: Dispatch<SidebarActionTypes>) => {
    return {
        toggleSidebar: () => dispatch(toggleSidebar())
    }
}

export default connect(null, mapDispatchToProps)(SidebarHeader);